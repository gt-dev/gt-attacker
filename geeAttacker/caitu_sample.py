#! /usr/bin/env python
# -*- coding=utf-8 -*-

import os
import numpy
import Image
import stat
import argparse 
import sys
import random
def encodeChinese(msg):
   type=sys.getfilesystemencoding()
   return msg.decode('UTF-8').encode(type)
image_path='ads_slice.png'
log_path='caitu.png'
help_image_path=encodeChinese(image_path)
help_log_path=encodeChinese(log_path)
help_path=encodeChinese('自动处理裁减贴图的工具')

parser = argparse.ArgumentParser(description=encodeChinese('自动处理裁减贴图的工具'))
parser.add_argument('--dir', action='store', dest='image_dir',
                    help=help_image_path) #common.encodeChinese(help_log_path))
parser.add_argument('--log', action='store', dest='log_filename',
                    help=help_log_path) #common.encodeChinese(help_log_path))
parser.add_argument('--version', action='version', version='%(prog)s 1.0')
args = parser.parse_args()

err_image_dir=encodeChinese('没有输入需要裁剪文件路径')
err_log_dir=encodeChinese('没有输入处理后文件保存路径')

if args.image_dir is None :
    print err_image_dir
    sys.exit()
if args.log_filename is None :
    print err_log_dir
    sys.exit()

rootdir=args.image_dir
targetDir=args.log_filename
for parent,dirnames,filenames in os.walk(rootdir):
   for filename in filenames:
      filename1,filename2 = os.path.split(filename)
      filename = rootdir+os.sep+filename
      fName,fPath = os.path.splitext(filename)
      value_r = 0
      value_g = 0
      value_b = 0
      count = 0
      if(fPath =='.png'):
         #os.chmod(filename,stat.S_IWRITE)
         img = Image.open(filename)
         if img.mode == 'RGBA':
            rgba = encodeChinese('这张是RGBA图片')
            print rgba
            img.load()
            data = img.getdata()
            data_a = list()
            for item in data:
               if item[3] < 128:
                  data_a.append((item[0],item[1],item[2],0))
               else:
                  data_a.append((item[0],item[1],item[2],255))
            for item in data:
               if item[3] == 255:
                  count += 1
                  value_r += item[0]
                  value_g += item[1]
                  value_b += item[2]
            value_r = value_r / count
            value_g = value_g / count
            value_b = value_b / count
            print value_r
            print value_g
            print value_b
            print count
            newdata = list()
            for item in data_a:
               if item[3] == 0:
                  newdata.append((value_r,value_g,value_b,item[3]))
               elif item[3] == 255:
                  newdata.append((item[0],item[1],item[2],item[3]))
            print len(newdata)
            img.putdata(newdata)
            img.save(targetDir + os.sep+filename2);                  
            print str(filename2) + ' save'
         #如果它没有alpha通道
         elif img.mode == 'RGB':
            print 'RGB'
            img.load()
            #转换出一个alpha通道
            img = img.convert('RGBA')
            datas = img.getdata()
            mydata = list()
            newdata = list()
            for item in data:
               if item[0] == 0 and item[1] == 0 and item[2] == 0:
                  mydata.append((item[0],item[1],item[2],0))
               else:
                  mydata.append((item[0],item[1],item[2],255))
            for item in data:
               if item[3] == 255:
                  count += 1
                  value_r += item[0]
                  value_g += item[1]
                  value_b += item[2]
            value_r = value_r / count
            value_g = value_g / count
            value_b = value_b / count
            for item in mydata:
               newdata.append((value_r,value_g,value_b,item[3]))
            img.putdata(newdata)
            img.save(targetDir + os.sep + filename2)
      #如果它不是png格式
      else:
         try:
            img = Image.open(filename)
            print 'JPG'
            img.load()
            #转换出一个alpha通道
            img = img.convert('RGBA')
            print 'convert'
            datas = img.getdata()
            print 'getdata()'
            mydata = list()
            newdata = list()
            for item in datas:
               if item[0] == 0 and item[1] == 0 and item[2] == 0:
                  mydata.append((item[0],item[1],item[2],0))
               else:
                  mydata.append((item[0],item[1],item[2],255))
            print 'loop data'
            for item in datas:
               if item[3] == 255:
                  count += 1
                  value_r += item[0]
                  value_g += item[1]
                  value_b += item[2]
            print 'value_r: ' + str(value_r)
            print 'value+g: ' + str(value_g)
            print 'value+b: ' + str(value_b)
            value_r = value_r / count
            value_g = value_g / count
            value_b = value_b / count
            print 'value_r / count : ' + str(value_r)
            print 'value_g / count : ' + str(value_g)
            print 'value_b / count " ' + str(value_b)
            for item in mydata:
               newdata.append((value_r,value_g,value_b,item[3])) 
            img.putdata(newdata)
            print 'putdata(newdata)'
            img.save(targetDir + os.sep + filename2)
            print img.mode
            print str(filename2)+'  save over'
         except:
            not_png = encodeChinese('这张贴图既不是png也不是jpg,这张贴图的名字是: ')
            print not_png + filename2
