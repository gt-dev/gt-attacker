#! /usr/bin/env python
# -*- coding=utf-8 -*-


# silence ****************************************
# import urllib2
# import gzip
# import StringIO

# url="file:///D:/Eric/geeAttacker/2.html"
# req_header = {'User-Agent':'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
# 'Accept':'text/html;q=0.9,*/*;q=0.8',
#              'Accept-Charset':'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
#              'Accept-Encoding':'gzip',
#              'Connection':'close',
#              'Referer':None #注意如果依然不能抓取的话，这里可以设置抓取网站的host
#              }
# req_timeout = 5
# req = urllib2.Request(url,None,req_header)
# resp = urllib2.urlopen(req,None,req_timeout)
# html = resp.read()
# resp.close()
# # html = gzip.GzipFile(fileobj=StringIO.StringIO(html), mode="r")
# # html = html.read().decode('utf-8').encode('utf-8')
# # html = html.read()
# gee_file = open("geetest_test.txt","w")
# gee_file.write(html)
# gee_file.close()


# js get ******************************************
# import sys,urllib2
# from HTMLParser import HTMLParser
# from PyQt4.QtCore import *
# from PyQt4.QtGui import *
# from PyQt4.QtWebKit import *
# class Render(QWebPage):
# 	def __init__(self, url):
# 		self.app = QApplication(sys.argv)
# 		QWebPage.__init__(self)
# 		self.loadFinished.connect(self._loadFinished)
# 		self.mainFrame().load(QUrl(url))
# 		self.app.exec_()
# 	def _loadFinished(self, result):
# 		self.frame = self.mainFrame()
# 		self.app.quit()
# url = './2.html'
# r = Render(url)
# html = r.frame.toHtml()
# print html.toUtf8()

# # 将执行后的代码写入文件中
# gee_file = open("geetest_test.txt","w")
# gee_file.write(html.toUtf8())
# gee_file.close()


# contral brower ========================================================
import os
import re
import urllib
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import time
from PIL import Image
import random


def delta_norm(a, b):
    result = 0
    for i in range(len(a)):
        result += ((a[i] - b[i]) ** 2)
    result = float(result) ** (0.5)
    return result


chromedriver = "/usr/bin/chromedriver"
os.environ["webdriver.chrome.driver"] = chromedriver
driver = webdriver.Chrome(chromedriver)
# driver.get("file:///D:/Eric/geeAttacker/2.html")

num = 0
beg_time = time.time()
while num < 5000000:
    try:
        #driver.get("file:///media/zheng/sd_expand/gt-workspace/gt-attacker/geeAttacker/2.html")
        driver.get("http://localhost:8000/static/geeAttacker/265gtest.html")
       #driver.get("http://192.168.1.39:8000/static/geeAttacker/265gtest.html")
        time.sleep(0.2)
        while True:
            item_ads = driver.find_element_by_class_name("gt_ads")
            url_ads = item_ads.get_attribute("style")
            url_ads = re.findall("\(.*?\)", url_ads)
            # print url_ads
            if url_ads == []:
                time.sleep(0.1)
                continue
            else:
                break

        url_ads = url_ads[0].strip("(")
        url_ads = url_ads.strip(")")
        data_ads = urllib.urlretrieve(url_ads, "ads.webp")
        im_ads = Image.open("ads.webp")
        size = im_ads.size
        pix_ads = im_ads.load()
        # im_ads.show()

        item_ads_bg = driver.find_element_by_class_name("gt_ads_bg")
        url_ads_bg = item_ads_bg.get_attribute("style")
        url_ads_bg = re.findall("\(.*?\)", url_ads_bg)
        url_ads_bg = url_ads_bg[0].strip("(")
        url_ads_bg = url_ads_bg.strip(")")
        data_ads_bg = urllib.urlretrieve(url_ads_bg, "ads_bg.webp")
        im_ads_bg = Image.open("ads_bg.webp")
        # print im_ads_bg.mode
        # print im_ads_bg.size
        pix_ads_bg = im_ads_bg.load()
        # im_ads_bg.show()


        right_ans = 0
        n = 0
        # print size
        diff = []
        for i in range(size[0]):
            for j in range(size[1]):
                if delta_norm(pix_ads[i, j], pix_ads_bg[i, j]) > 100:
                    diff.append([delta_norm(pix_ads[i, j], pix_ads_bg[i, j]), i])
                    right_ans += i
                    n += 1
        # fl = open("data.txt","w")
        # for i in diff:
        # 	fl.write(str(i) + "\n")
        # fl.close()
        # print n
        right_ans = right_ans / n - 30
        print right_ans
        num += 1
        temp_time = time.time()
        print str(num) + "\t" + str("%.2f" % (float(temp_time - beg_time) / float(60))) + "min\t\t" + str(
            "%.2f" % (float(temp_time - beg_time) / float(num))) + "sec/pass\t" + str(
            "%.2f" % (float(num) / (float(temp_time - beg_time) / float(60)))) + "pass/min"

        driver.execute_script("""
					var simulate = function (answer, useTrack) {
			  var sp = {
			      x: Math.random() * 80,
			      y: Math.random() * 25
			    }, //start point
			    knob = document.querySelector(".gt_slider_knob"),
			    pos = knob.getBoundingClientRect(),
			    x = pos.left + sp.x,
			    y = pos.top + sp.y;
			  var trigger = function (type, x, y, elem) {
			      var tempEvent = document.createEvent("MouseEvents");
			      tempEvent.initMouseEvent(type, true, true, window, 0, x, y, x, y, false, false, false, false, 0, null);
			      elem.dispatchEvent(tempEvent);
			      //mouse trigger function
			    },
			    mouseMove = function (x, y, elem) {
			      elem = elem || document;
			      trigger("mousemove", x, y, elem)
			    },
			    mouseDown = function (x, y, elem) {
			      elem = elem || knob;
			      trigger("mousedown", x, y, elem)
			    },
			    mouseUp = function (x, y, elem) {
			      elem = elem || document;
			      trigger("mouseup", x, y, elem)
			    };
			  var trackLib = {
			    'randomLine': {
			      x: function (e) {
			        return e.i / e.step * e.answer;
			      },
			      y: function (e) {
			        return Math.random() * 5 - 10;
			      },
			      t: function (e) {
			        return e.t + e.time / e.step * (0.5 + Math.random());
			      },
			      step: function () {
			        return Math.random() * 40 + 30;
			      },
			      time: function () {
			        return 600 + 600 * Math.random();
			      }
			    },
			    'simpleSimulate': {
			      x: function (e) {
			        var gx = function (x) {
			          return 1 / 4 * Math.pow(x, 4) - 5 / 3 * Math.pow(x, 3) + 2 * Math.pow(x, 2)
			        }
			        var jx = function (x) {
			          return gx(x) / gx(1)
			        }
			        return e.answer * jx(e.i / e.step);
			      },
			      y: function (e) {
			        return Math.random() * 2 - 2;
			      },
			      t: function (e) {
			        var dt;
			        if (e.i < 2) {
			          dt = Math.random() * 20 + 40
			        }
			        else {
			          dt = 16 + Math.random() * 2;
			        }
			        return e.t + dt;
			      },
			      delay: function () {
			        return 0;
			      },
			      step: function (e) {
			        var time = e.time;
			        return time / 60;
			      },
			      time: function (e) {
			        return e.answer * 20
			      }
			    },
			    'pow': {
			      x: function (e) {
			        var value = 2;
			        return Math.pow(e.i / e.step, value) * e.answer;
			      },
			      y: function () {
			        return 0;
			      },
			      t: function (e) {
			        var dt;
			        if (e.i < 3) {
			          dt = Math.random() * 20 + 50
			        }
			        else {
			          dt = 16 + Math.random() * 2;
			        }
			        return e.t + dt;
			      },
			      step: function (e) {
			        var time = e.time;
			        return time / 15;
			      },
			      delay: function () {
			        return 80 + Math.random() * 20;
			      },
			    },
			    'squre': {
			      x: function (e) {
			        if(e.i == e.step) {
			          return e.answer;
			        }
			        return e.i / e.step * e.answer * Math.random()
			      },
			      y: function (e) {
			        return Math.random() * 20
			      },
			      t: function (e) {
			        return e.t + e.time / e.step;
			      }
			    },
			    'stupidLine': {
			      x: function (e) {
			        return e.i / e.step * e.answer;
			      },
			      y: function (e) {
			        return 0;
			      },
			      t: function (e) {
			        return e.t + e.time / e.step;
			      },
			      step: function () {
			        return Math.random() * 100 + 3
			      },
			      delay: function () {
			        return 0;
			      },
			      time: function () {
			        return 10
			      },
			      answer: function () {
			        return 80
			      }
			    },
			    'human_simulation': {
			      x: function (e) {
			        var value = 3 / 5;
			        return Math.sin(e.i / e.step * value * Math.PI) * e.answer / Math.sin(value * Math.PI);
			      },
			      y: function (e) {
			        return e.y + Math.random() * 20 - 20;
			      },
			      t: function (e) {
			        var dt = parseInt(Math.random() * 5 + e.time / e.step - 5);
			        return e.t + dt;
			      },
			      delay: function (e) {
			        return Math.random() * 200 + 200;
			      },
			      step: function (e) {
			        return parseInt(Math.random() * 20 + 60);
			      },
			      time: function () {
			        return parseInt(Math.random() * 500 + 2200);
			      }
			    }
			  };

			  var init = function (answer) {
			    var track = trackLib[useTrack] || trackLib.sin,
			      t = 0,
			      answer = answer || Math.random() * 140 + 30,
			      time = parseInt(track.time ? track.time({answer: answer}) : Math.random() * 1000 + 1000),
			      step = parseInt(track.step ? track.step({time: time}) : Math.random() * 10 + 60),
			      delay = parseInt(track.delay ? track.delay() : Math.random() * 200 + 200);

			    mouseDown(x, y);
			    var tempX = x,
			      tempY = y;
			    var move = function (i) {
			      t = track.t({
			        t: t,
			        i: i,
			        step: step,
			        time: time
			      });
			      setTimeout(function () {
			        tempX = parseInt(track.x({
			          i: i,
			          step: step,
			          answer: answer
			        }));
			        tempY = parseInt(track.y({
			          y: tempY
			        }));
			        mouseMove(x + tempX, y + tempY);
			        if (i == step - 1) {
			          setTimeout(function () {
			            mouseUp(tempX + x, tempY + y)
			          }, delay);
			        }
			      }, t);
			    };
			    for (var i = 0; i < step; i++) {
			      move(i + 1);
			    }
			  };
			  init(answer);
			  return "Finish";
			};

			var RealAnser = arguments[0];

			var goSimulate = function () {
			  simulate(RealAnser, 'stupidLine');
			};
			goSimulate();


			""", right_ans)
        # "randomLine"
        # "simpleSimulate"
        # "pow"
        # "squre"
        # "stupidLine"
        # "human_simulation"
        # item_ads_slice = driver.find_element_by_class_name("gt_ads_slice")
        # url_ads_slice = item_ads_slice.get_attribute("style")
        # url_ads_slice = re.findall("\(.*?\)",url_ads_slice)
        # url_ads_slice = url_ads_slice[0].strip("(")
        # url_ads_slice = url_ads_slice.strip(")")
        # data_ads_slice = urllib.urlretrieve(url_ads_slice,"ads_slice.png")
        # im_ads_slice = Image.open("ads_slice.png")
        # im_ads_slice.show()
        # print im_ads_slice.mode
        # print im_ads_slice.size
        # pix_ads_slice = im_ads_slice.load()
        # for i in range(im_ads_slice.size[0]):
        # 	print pix_ads_slice[i,23]
        # ActionChains(driver).drag_and_drop(knob, target).perform()
        time.sleep(0.5)
        driver.refresh()
        time.sleep(0.1)

    except:
        continue

time.sleep(1)
driver.close()
# driver.quit()


# -*- coding: utf-8 -*-

# from selenium import webdriver
# import time
# import os
# def capture(url, save_fn="capture.png"):
# 	chromedriver = "C:/Python27/driver/chromedriver.exe"
# 	os.environ["webdriver.chrome.driver"] = chromedriver
# 	browser =  webdriver.Chrome(chromedriver)
# 	browser.set_window_size(1200, 900)
# 	browser.get(url) # Load page
# browser.execute_script("""
#   (function () {
#     var y = 0;
#     var step = 100;
#     window.scroll(0, 0);
#     function f() {
#       if (y < document.body.scrollHeight) {
#         y += step;
#         window.scroll(0, y);
#         setTimeout(f, 50);
#       } else {
#         window.scroll(0, 0);
#         document.title += "scroll-done";
#       }
#     }
#     setTimeout(f, 1000);
#   })();
# """)
# 	for i in xrange(30):
# 		if "scroll-done" in browser.title:
# 			break
# 		time.sleep(1)
# 	browser.save_screenshot(save_fn)
# 	browser.close()


# if __name__ == "__main__":

#   capture("http://www.jb51.net")


