var simulate = function (answer, useTrack) {
  var sp = {
      x: Math.random() * 80,
      y: Math.random() * 25
    }, //start point
    knob = document.querySelector(".gt_slider_knob"),
    pos = knob.getBoundingClientRect(),
    x = pos.left + sp.x,
    y = pos.top + sp.y;
  var trigger = function (type, x, y, elem) {
      var tempEvent = document.createEvent("MouseEvents");
      tempEvent.initMouseEvent(type, true, true, window, 0, x, y, x, y, false, false, false, false, 0, null);
      elem.dispatchEvent(tempEvent);
      //mouse trigger function
    },
    mouseMove = function (x, y, elem) {
      elem = elem || document;
      trigger("mousemove", x, y, elem)
    },
    mouseDown = function (x, y, elem) {
      elem = elem || knob;
      trigger("mousedown", x, y, elem)
    },
    mouseUp = function (x, y, elem) {
      elem = elem || document;
      trigger("mouseup", x, y, elem)
    };
  var trackLib = {
    'randomLine': {
      x: function (e) {
        return e.i / e.step * e.answer;
      },
      y: function (e) {
        return Math.random() * 5 - 10;
      },
      t: function (e) {
        return e.t + e.time / e.step * (0.5 + Math.random());
      },
      step: function () {
        return Math.random() * 40 + 30;
      },
      time: function () {
        return 600 + 600 * Math.random();
      }
    },
    'simpleSimulate': {
      x: function (e) {
        var gx = function (x) {
          return 1 / 4 * Math.pow(x, 4) - 5 / 3 * Math.pow(x, 3) + 2 * Math.pow(x, 2)
        }
        var jx = function (x) {
          return gx(x) / gx(1)
        }
        return e.answer * jx(e.i / e.step);
      },
      y: function (e) {
        return Math.random() * 2 - 2;
      },
      t: function (e) {
        var dt;
        if (e.i < 2) {
          dt = Math.random() * 20 + 40
        }
        else {
          dt = 16 + Math.random() * 2;
        }
        return e.t + dt;
      },
      delay: function () {
        return 0;
      },
      step: function (e) {
        var time = e.time;
        return time / 60;
      },
      time: function (e) {
        return e.answer * 20
      }
    },
    'pow': {
      x: function (e) {
        var value = 2;
        return Math.pow(e.i / e.step, value) * e.answer;
      },
      y: function () {
        return 0;
      },
      t: function (e) {
        var dt;
        if (e.i < 3) {
          dt = Math.random() * 20 + 50
        }
        else {
          dt = 16 + Math.random() * 2;
        }
        return e.t + dt;
      },
      step: function (e) {
        var time = e.time;
        return time / 15;
      },
      delay: function () {
        return 80 + Math.random() * 20;
      },
    },
    'squre': {
      x: function (e) {
        if(e.i == e.step) {
          return e.answer;
        }
        return e.i / e.step * e.answer * Math.random()
      },
      y: function (e) {
        return Math.random() * 20
      },
      t: function (e) {
        return e.t + e.time / e.step;
      }
    },
    'stupidLine': {
      x: function (e) {
        return e.i / e.step * e.answer;
      },
      y: function (e) {
        return 0;
      },
      t: function (e) {
        return e.t + e.time / e.step;
      },
      step: function () {
        return Math.random() * 100 + 3
      },
      delay: function () {
        return 500 * Math.random();
      },
      time: function () {
        return 500 + 2000 * Math.random();
      },
      answer: function () {
        return 30 + 120 * Math.random();
      }
    },
    'sin': {
      x: function (e) {
        var value = 3 / 5;
        return Math.sin(e.i / e.step * value * Math.PI) * e.answer / Math.sin(value * Math.PI);
      },
      y: function (e) {
        return e.y + Math.random() * 20 - 20;
      },
      t: function (e) {
        var dt = parseInt(Math.random() * 5 + e.time / e.step - 5);
        return e.t + dt;
      },
      delay: function (e) {
        return Math.random() * 200 + 200;
      },
      step: function (e) {
        return parseInt(Math.random() * 20 + 60);
      },
      time: function () {
        return parseInt(Math.random() * 500 + 2200);
      }
    }
  };

  var init = function (answer) {
    var track = trackLib[useTrack] || trackLib.sin,
      t = 0,
      answer = answer || Math.random() * 140 + 30,
      time = parseInt(track.time ? track.time({answer: answer}) : Math.random() * 1000 + 1000),
      step = parseInt(track.step ? track.step({time: time}) : Math.random() * 10 + 60),
      delay = parseInt(track.delay ? track.delay() : Math.random() * 200 + 200);

    mouseDown(x, y);
    var tempX = x,
      tempY = y;
    var move = function (i) {
      t = track.t({
        t: t,
        i: i,
        step: step,
        time: time
      });
      setTimeout(function () {
        tempX = parseInt(track.x({
          i: i,
          step: step,
          answer: answer
        }));
        tempY = parseInt(track.y({
          y: tempY
        }));
        mouseMove(x + tempX, y + tempY);
        if (i == step - 1) {
          setTimeout(function () {
            mouseUp(tempX + x, tempY + y)
          }, delay);
        }
      }, t);
    };
    for (var i = 0; i < step; i++) {
      move(i + 1);
    }
  };
  init(answer);
  return "Finish";
};

var cur_mod = 'randomLine';


var goSimulate = function () {
  simulate(0, cur_mod);
};

var stop = false;
var gt_custom_ajax = function (result, selector, message) {
  if (!stop) {
    setTimeout(function () {
      selector(".gt_refresh_button").click();
    }, 1000);
  }
};

var gt_custom_refresh = function () {
  goSimulate();
};

goSimulate();

// Change mod every 30 minutes

var mod_arr = ['randomLine', 'simpleSimulate', 'pow', 'squre', 'stupidLine', 'sin'];
var cur_idx = 0;

setInterval(function () {
  cur_mod = mod_arr[cur_idx++%6];
}, 10000);
