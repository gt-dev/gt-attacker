function d(first) {
  $('.gt_slider_knob').simulate('drag', { dx: first ? 50 : 8 });
  setTimeout(function(){d(false);}, 1000);
}

window.gt_custom_ajax = function(result, selector) {  
  if(result) {
    setTimeout(function() {
      selector('.gt_refresh_button').click();
    }, 200)
  }
}
 
jQuery.getScript("http://test.geetest.com/jquery.simulate.js", function() {
  d(true);
});
